package com.citi.pricing;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.logging.Logger;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("prices")
@CrossOrigin(origins="*")
public class PriceService {

	private static Logger LOG = Logger.getLogger(PriceService.class.getName());
	
    private static Map<String,Function<Double,TrackedStock>> profiles = new HashMap<>();
    static {
        profiles.put("AA", TrackedStock.PROFILE4);
        profiles.put("AAPL", TrackedStock.PROFILE1);
        profiles.put("BG", TrackedStock.PROFILE1);
        profiles.put("BOM", TrackedStock.PROFILE5);
        profiles.put("GOOG", TrackedStock.PROFILE1);
        profiles.put("HON", TrackedStock.PROFILE2);
        profiles.put("MRK", TrackedStock.PROFILE3);
        profiles.put("MSFT", TrackedStock.PROFILE2);
        profiles.put("NUAN", TrackedStock.PROFILE2);
        profiles.put("OLN", TrackedStock.PROFILE2);
        profiles.put("REMX", TrackedStock.PROFILE1);
        profiles.put("RIO", TrackedStock.PROFILE1);
        profiles.put("SIEGY", TrackedStock.PROFILE2);
    }
    
    private BasePrices basePrices;
    
    private Map<String,TrackedStock> tracked = new HashMap<>();
    
    public PriceService(BasePrices basePrices) {
        this.basePrices = basePrices;
    }
    
    public TrackedStock getTrackedStock(String symbol) {
        if (!tracked.containsKey(symbol)) {
            Function<Double,TrackedStock> profile = profiles.get(symbol);
            if (profile == null) {
                profile = TrackedStock.PROFILE1;
            }
            
            double basePrice = basePrices.containsKey(symbol) 
                ? basePrices.get(symbol)
                : 60;
            
            tracked.put(symbol, profile.apply(basePrice));
        }
        
        return tracked.get(symbol);
    }
    
    @RequestMapping(method=RequestMethod.GET, value="{symbol}", produces="text/csv")
    @ResponseStatus(HttpStatus.OK)
    public String getPriceData(@PathVariable("symbol") String symbol, 
    		@RequestParam("periods") int periods) {
    	LOG.info("Requested " + symbol + " (" + periods + " periods).");
        return getTrackedStock(symbol).getLatestData(periods);
    }
    
    public static void main(String[] args) throws IOException {
        BasePrices basePrices = new BasePrices("BasePrices.csv");
        PriceService service = new PriceService(basePrices);
        
        try ( PrintWriter out = new PrintWriter(new FileWriter("Prices.csv")); ) {
            out.println(service.getPriceData("MSFT", 120));
        }
    }
}
