import { Component, ElementRef, OnDestroy, OnInit } from "@angular/core";
import { ModalService } from "./modal-service";

/**
 * Base class for a modal component.
 */
export class Modal implements OnInit, OnDestroy {
  id: string;
  root: any;
  body: any;
  modalService: ModalService;

  /**
   * Stores off the service reference, the native HTML root element,
   * and this modal's HTML ID, hides the root element,
   * and initializes references to the body element of the modal template.
   */
  constructor(modalService: ModalService, elementRef: ElementRef, id: string) {
    this.modalService = modalService;
    this.root = elementRef.nativeElement;
    this.id = id;

    this.root.style.display = "none";
    this.body = this.root.querySelector(".t-modal-body");
  }

  /**
   * Sets a click listener so that we're hidden again on a click
   * in the background. Adds this modal to the service.
   */
  ngOnInit() {
    this.root.addEventListener("click", ev => {
        if (ev.target.className === "t-modal") {
          this.close();
        }
      });
    this.modalService.add(this);
  }

  /**
   * Removes this modal from the service.
   */
  ngOnDestroy() {
    this.modalService.remove(this.id);
  }

  /**
   * Shows the modal and sets background syling on the HTML body.
   */
  open() {
    this.root.style.display = "block";
    document.body.classList.add("t-modal-open");
  }

  /**
   * Hides the modal and resets background syling on the HTML body.
   */
  close() {
    this.root.style.display = "none";
   document.body.classList.remove("t-modal-open");
  }
}
