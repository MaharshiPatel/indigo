package com.citi.trading.strategy;

import static com.citi.trading.pricing.Pricing.SECONDS_PER_PERIOD;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import com.citi.trading.OrderPlacer;
import com.citi.trading.pricing.PriceData;
import com.citi.trading.pricing.PricePoint;
import com.citi.trading.pricing.PricingSource;

@Component
@Scope(ConfigurableBeanFactory.SCOPE_PROTOTYPE)
public class BollingerBandsTrader extends Trader<BollingerBands> {

	private static final Logger LOGGER =
	        Logger.getLogger(BollingerBands.class.getName ());
	
	private boolean isBound = true;
	private boolean isUndervalued;
	
	public BollingerBandsTrader(PricingSource pricing, 
    		OrderPlacer market, StrategyPersistence strategyPersistence) {
		super(pricing, market, strategyPersistence);
	}
	
	protected int periods(int msec) {
    	return msec / 1000 / SECONDS_PER_PERIOD;
    }
    
    public int getNumberOfPeriodsToWatch() {
    	return periods(strategy.getWindowSize());
    }
    
    /**
     * Helper method to calculate the standard deviation and update the flags
     */
    private void checkStdev(PriceData data, double currentPrice) {
        int numberOfPoints = getNumberOfPeriodsToWatch();
        double avg = data.getWindowAverage(
        		numberOfPoints, PricePoint::getClose);
        double stdev = data.getWindowStdev(
        		numberOfPoints, PricePoint::getClose, avg);
    	
        // SEVERE error check: negative stock price average
        if (avg < 0.0) {
        	LOGGER.log(Level.SEVERE, "PriceData is reporting negative stock price averages!");
        }
        
        // <--|...avg...|-->
        isBound = ( currentPrice < (avg + Math.abs(strategy.getStdevMultiple()*stdev)) )	// less than max
        			&&
        			( currentPrice > (avg - Math.abs(strategy.getStdevMultiple()*stdev)) );	// greater than min
        isUndervalued = currentPrice < (avg - Math.abs(strategy.getStdevMultiple()*stdev));
    	
    	LOGGER.fine(String.format("Trader " + strategy.getId() + " as of %s average is %1.4f, standard devication is %1.4f, therefore %s",
    			data.getLatestTimestamp(), avg, stdev, 
    			isBound ? "***Wait***" : (isUndervalued ? "***Long***" : "***Short***")));
    }

    /**
     * When we're open, just check to see if the latest closing price is a profit or
     * loss greater than our configured threshold. If it is, use the base class'
     * <strong>Closer</strong> to close the position.
     */
	protected void handleDataWhenOpen(PriceData data) {
		double currentPrice = data.getData(1).findAny().get().getClose();
		double openingPrice = getStrategy().getOpenPosition().getOpeningTrade().getPrice();
		double profitOrLoss = currentPrice / openingPrice - 1.0;
    	
    	if (Math.abs(profitOrLoss) > strategy.getExitThreshold()) {
    		closer.placeOrder(currentPrice);
    		if (!getStrategy().getOpenPosition().getOpeningTrade().isBuy()) {
    			profitOrLoss = 0 - profitOrLoss;
    		}
    		LOGGER.info(String.format("Trader " + strategy.getId() + " closing position on a profit/loss of %5.3f percent.", 
    				profitOrLoss * 100));
    	}
	}

	/**
     * When we're closed, capture the most recent isBound/isUndervalued flags,
     * update to get the new ones, and see if there's been a change.
     * If under-valued, buy to open a long position;
     * if over-valued, sell to open a short position.
     */
	protected void handleDataWhenClosed(PriceData data) {
		
		if (tracking.get()) {
    		double currentPrice = data.getData(1).findAny().get().getClose();
    		checkStdev(data, currentPrice);
    		
    		if (isBound == false) {
    			opener.placeOrder(isUndervalued, currentPrice);
    			LOGGER.info("Trader " + strategy.getId() + " opening position to " + (isUndervalued ? "buy" : "sell") + ", as prices hit %1.4f times " +
    					(isUndervalued ? "under" : "over") + " the standard deviation.");
    		}
    		
	    } else if (data.getSize() >= getNumberOfPeriodsToWatch()) {
	    	double currentPrice = data.getData(1).findAny().get().getClose();
    		checkStdev(data, currentPrice);
    		
	    	tracking.set(true);
	    	LOGGER.info("Trader " + strategy.getId() + " got initial pricing data, baseline average and standard deviation.");
	    }
	}
}
