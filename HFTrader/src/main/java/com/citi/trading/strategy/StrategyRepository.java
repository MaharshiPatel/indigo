package com.citi.trading.strategy;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

/**
 * Spring Data repository for {@link Strategy} objects. 
 * 
 * @author Will Provost
 */
public interface StrategyRepository extends CrudRepository<Strategy,Integer> {
	
	/**
	 * Similar to <strong>findById()</strong> but assures eager fetching of
	 * all of the associated {@link Position}s and {@link Trade}s.
\	 */
	@Query("select s from Strategy s left join fetch s.positions where s.id = :ID")
	public Strategy findStrategyAndPositions(@Param("ID") int ID);

	/**
	 * Query for all of the active strategies in the database, including 
	 * eager fetching of all of the associated {@link Position}s and {@link Trade}s.
	 * @return
	 */
	@Query("select distinct s from Strategy s left join fetch s.positions where s.active=true")
	public List<Strategy> findActiveStrategies();
}
