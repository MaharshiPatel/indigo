(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/app/app.component.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/app/app.component.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<h1><img style=\"float: right;\" src=\"../assets/citigrouplogo.jpg\" height=\"80px\" >{{title}}</h1>\r\n<trader-toolbar></trader-toolbar>\r\n<hr width=\"100%\" />\r\n<trader-table></trader-table>\r\n\r\n<router-outlet></router-outlet>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/view/trader-table.html":
/*!**************************************************************!*\
  !*** ./node_modules/raw-loader!./src/view/trader-table.html ***!
  \**************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<table>\r\n  <thead>\r\n    <tr>\r\n      <th>Stock</th>\r\n      <th class=\"right\" >Size</th>\r\n      <th class=\"right\" >Short</th>\r\n      <th class=\"right\" >Long</th>\r\n      <th class=\"right\" >Exit</th>\r\n      <th>State</th>\r\n      <th class=\"right\" >Trades</th>\r\n      <th class=\"right\" >Profit</th>\r\n      <th class=\"right\" >ROI</th>\r\n    </tr>\r\n  </thead>\r\n  <tbody>\r\n    <tr\r\n      *ngFor=\"let trader of traders; let i = index\"\r\n      [ngClass]=\"{\r\n          'oddWinner': trader.profitOrLoss > 0 && i % 2 == 1,\r\n          'evenWinner': trader.profitOrLoss > 0 && i % 2 == 0,\r\n          'oddLoser': trader.profitOrLoss < 0 && i % 2 == 1,\r\n          'evenLoser': trader.profitOrLoss < 0 && i % 2 == 0\r\n        }\"\r\n    >\r\n      <td>{{trader.stock}}</td>\r\n      <td class=\"right\" >{{trader.size}}</td>\r\n      <td class=\"right\" >{{minSec(trader.lengthShort)}}</td>\r\n      <td class=\"right\" >{{minSec(trader.lengthLong)}}</td>\r\n      <td class=\"right\" >{{trader.exitThreshold | percent:'1.2-2'}}</td>\r\n      <td>\r\n        <input\r\n          id=\"trader{{i}}\"\r\n          type=\"button\"\r\n          [value]=\"getState(trader)\"\r\n          [disabled]=\"getState(trader) == 'Stopping'\"\r\n          (click)=\"startOrStop($event, false)\" />\r\n      </td>\r\n      <td class=\"right indent\" >{{trader.trades}}</td>\r\n      <td *ngIf=\"trader.trades > 1\" class=\"right\" >{{trader.profitOrLoss | currency:'USD':'symbol':'1.2-2'}}</td>\r\n      <td *ngIf=\"trader.trades > 1\" class=\"right\" >{{trader.ROI | percent:'1.4-4'}}</td>\r\n    </tr>\r\n   </tbody>\r\n   <tfoot>\r\n     <tr>\r\n       <td colspan=\"7\" ></td>\r\n       <td class=\"right indent\" >{{getTotalTrades()}}</td>\r\n       <td class=\"right\" >{{getTotalProfit() | currency:'USD':'symbol':'1.2-2'}}</td>\r\n     </tr>\r\n   </tfoot>\r\n</table>\r\n"

/***/ }),

/***/ "./node_modules/raw-loader/index.js!./src/view/trader-toolbar.html":
/*!****************************************************************!*\
  !*** ./node_modules/raw-loader!./src/view/trader-toolbar.html ***!
  \****************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div>\r\n  <span>Type:</span>\r\n  <select value=\"{{type}}\" >\r\n    <option value=\"2MA\" >Two moving averages</option>\r\n    <option value=\"BB\" >Bollinger bands</option>\r\n  </select>\r\n  <span>Stock:</span>\r\n  <input type=\"text\" [(ngModel)]=\"stock\" />\r\n  <span>Size:</span>\r\n  <input type=\"number\" step=\"100\" style=\"width: 56px;\" [(ngModel)]=\"size\" />\r\n  <input type=\"button\" value=\"Create\" (click)=\"create()\" />\r\n  <br/>\r\n  <!-- TODO: content here varies by trader type -->\r\n  <span>Short:</span>\r\n  <input type=\"number\" min=\"15\" step=\"15\" [(ngModel)]=\"lengthShort\" />\r\n  <span>Long:</span>\r\n  <input type=\"number\" min=\"15\" step=\"15\" [(ngModel)]=\"lengthLong\" />\r\n  <span>Exit:</span>\r\n  <input type=\"number\" min=\"0.25\" step=\"0.25\" [(ngModel)]=\"exitThreshold\" />%\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm2015/router.js");



const routes = [];
let AppRoutingModule = class AppRoutingModule {
};
AppRoutingModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
        imports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"].forRoot(routes)],
        exports: [_angular_router__WEBPACK_IMPORTED_MODULE_2__["RouterModule"]]
    })
], AppRoutingModule);



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "/* No styles at this level */\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUEsNEJBQTRCIiwiZmlsZSI6InNyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCJzb3VyY2VzQ29udGVudCI6WyIvKiBObyBzdHlsZXMgYXQgdGhpcyBsZXZlbCAqL1xyXG4iXX0= */"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        this.title = "Automated Trading Platform";
    }
};
AppComponent = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "app-root",
        template: __webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/index.js!./src/app/app.component.html"),
        styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm2015/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm2015/forms.js");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _view_trader_table__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../view/trader-table */ "./src/view/trader-table.ts");
/* harmony import */ var _view_trader_toolbar__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../view/trader-toolbar */ "./src/view/trader-toolbar.ts");








let AppModule = class AppModule {
};
AppModule = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_2__["NgModule"])({
        declarations: [
            _app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"],
            _view_trader_table__WEBPACK_IMPORTED_MODULE_6__["TraderTable"],
            _view_trader_toolbar__WEBPACK_IMPORTED_MODULE_7__["TraderToolbar"]
        ],
        imports: [
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_1__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_4__["AppRoutingModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_3__["FormsModule"]
        ],
        providers: [],
        bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_5__["AppComponent"]]
    })
], AppModule);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm2015/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "./src/model/trader-service.ts":
/*!*************************************!*\
  !*** ./src/model/trader-service.ts ***!
  \*************************************/
/*! exports provided: TraderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TraderService", function() { return TraderService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _two_moving_averages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./two-moving-averages */ "./src/model/two-moving-averages.ts");

var TraderService_1;


/**
 * A client component for the HFTrader application's TraderService.
 * Also has the responsibility to set a timer and to poll the service
 * for updates on traders and their status and trading history.
 * Subscribe with an implementation of the above TraderUpdate interface,
 * and this component will push updates as it fetches them.
 *
 * @author Will Provost
 */
let TraderService = TraderService_1 = class TraderService {
    /**
     * Set up service URL. Bind all methods so our 'this' references
     * make sense. Initialize subscribers array. Start the polling timer.
     */
    constructor() {
        this.URL = "/traders";
        this.handler = this.handler.bind(this);
        this.checkResponseCode = this.checkResponseCode.bind(this);
        this.checkOK = this.checkOK.bind(this);
        this.checkCreated = this.checkCreated.bind(this);
        this.getTraders = this.getTraders.bind(this);
        this.getTrader = this.getTrader.bind(this);
        this.setActive = this.setActive.bind(this);
        this.createTrader = this.createTrader.bind(this);
        this.subscribers = [];
        this.start();
    }
    /**
     * Add the subscriber.
     */
    subscribe(subscriber) {
        this.subscribers.push(subscriber);
    }
    /**
     * Remove the subscriber.
     */
    unsubscribe(subscriber) {
        for (let i = 0; i < this.subscribers.length; ++i) {
            if (this.subscribers[i] === subscriber) {
                this.subscribers.splice(i, 1);
                break;
            }
        }
    }
    /**
     * Call each subscriber, passing the current traders array.
     */
    notify() {
        this.getTraders().then(traders => {
            for (const subscriber of this.subscribers) {
                subscriber.latestTraders(traders);
            }
        });
    }
    /**
     * Set a timer to go off in one second and then every 15 seconds therafter,
     * calling our notify() method each time.
     */
    start() {
        const oneSecond = setInterval(() => {
            this.notify();
            this.timer = setInterval(this.notify.bind(this), 15000);
            clearInterval(oneSecond);
        }, 1000);
    }
    /**
     * Shut down the polling timer.
     */
    stop() {
        clearInterval(this.timer);
    }
    /**
     * Error handler currently just logs to the console.
     * A popup message box or some other UI should come into play at some point.
     */
    handler(err) {
        console.log(err);
        return null;
    }
    /**
     * Helper to check that the HTTP response code was the expected alue.
     * Either throws an error or returns the response, making the function
     * suitable for use in a fetch/then chain.
     */
    checkResponseCode(response, expected) {
        if (response.status !== expected) {
            throw Error("Unexpected response code: " + response.status);
        }
        return response;
    }
    /**
     * Specialization of checkResponseCode() that expects HTTP 200 OK.
     */
    checkOK(response) {
        return this.checkResponseCode(response, 200);
    }
    /**
     * Specialization of checkResponseCode() that expects HTTP 201 Created.
     */
    checkCreated(response) {
        return this.checkResponseCode(response, 201);
    }
    /**
     * Parses the given (weakly typed) object and creates the appropriate
     * type of Trader, holding the appropriate values.
     * Currently, only 2MA traders are supported.
     */
    static makeTrader(source) {
        if (source["@type"] === "2MA") {
            return new _two_moving_averages__WEBPACK_IMPORTED_MODULE_2__["TwoMovingAverages"](source.id, source.stock, source.size, source.active, !source.active && !source.stoppedTrading, source.positions, source.profitOrLoss, source.roi, source.lengthShort, source.lengthLong, source.exitThreshold);
        }
        else if (source["@type"] === "BB") {
            throw Error("BollingerBands NYI.");
        }
        else {
            throw Error("Unknown trader type: " + source["@type"]);
        }
    }
    /**
     * Calls the HTTP operation and returns a Promise bearing an array of
     * Trader objects.
     */
    getTraders() {
        return fetch(this.URL)
            .then(this.checkOK)
            .then(response => response.json())
            .then(traders => traders.map(TraderService_1.makeTrader))
            .catch(this.handler);
    }
    /**
     * Calls the HTTP operation and returns a Promise bearing the
     * requested trader object.
     */
    getTrader(ID) {
        return fetch(this.URL + "/" + ID)
            .then(this.checkOK)
            .then(response => response.json())
            .then(TraderService_1.makeTrader)
            .catch(this.handler);
    }
    /**
     * Calls the HTTP operation, and on successful response triggers
     * a notify() which in turn will fetch an updated list of traders.
     */
    setActive(ID, start) {
        return fetch(this.URL + "/" + ID + "/active", {
            method: "PUT",
            body: start ? "true" : "false"
        })
            .then(this.checkOK)
            .then(response => { this.notify(); return response; })
            .catch(this.handler);
    }
    /**
     * Calls the HTTP operation and returns a Promise bearing
     * the newly created trader, which will carry the server-generated ID.
     */
    createTrader(trader) {
        return fetch(this.URL, {
            method: "POST",
            headers: {
                "Content-Type": "application/json",
                Accept: "application/json",
            },
            body: JSON.stringify(trader)
        })
            .then(this.checkCreated)
            .then(response => response.json())
            .then(TraderService_1.makeTrader)
            .then(created => { this.notify(); return created; })
            .catch(this.handler);
    }
};
TraderService = TraderService_1 = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root',
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [])
], TraderService);



/***/ }),

/***/ "./src/model/trader.ts":
/*!*****************************!*\
  !*** ./src/model/trader.ts ***!
  \*****************************/
/*! exports provided: Trader */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Trader", function() { return Trader; });
/**
 * Serializable/deserializable encapsulation of a strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
class Trader {
    /**
     * Store all of the given information as properties,
     * and pre-compute the total number of trades for use by the HTML template.
     */
    constructor(typeName, ID, stock, size, active, stopping, positions, profitOrLoss, ROI) {
        this["@type"] = typeName;
        this.ID = ID;
        this.stock = stock;
        this.size = size;
        this.active = active;
        this.stopping = stopping;
        this.positions = positions;
        this.profitOrLoss = profitOrLoss;
        this.ROI = ROI;
        this.trades = positions.length * 2;
        if (positions.length !== 0 && !positions[positions.length - 1].closingTrade) {
            this.trades -= 1;
        }
    }
}


/***/ }),

/***/ "./src/model/two-moving-averages.ts":
/*!******************************************!*\
  !*** ./src/model/two-moving-averages.ts ***!
  \******************************************/
/*! exports provided: TwoMovingAverages */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TwoMovingAverages", function() { return TwoMovingAverages; });
/* harmony import */ var _trader__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./trader */ "./src/model/trader.ts");

/**
 * Serializable/deserializable encapsulation of a 2MA strategy,
 * including its trading history, profitability, and ROI.
 *
 * @author Will Provost
 */
class TwoMovingAverages extends _trader__WEBPACK_IMPORTED_MODULE_0__["Trader"] {
    constructor(ID, stock, size, active, stopping, positions, profitOrLoss, ROI, lengthShort, lengthLong, exitThreshold) {
        super("2MA", ID, stock, size, active, stopping, positions, profitOrLoss, ROI);
        this.lengthShort = lengthShort;
        this.lengthLong = lengthLong;
        this.exitThreshold = exitThreshold;
    }
}


/***/ }),

/***/ "./src/view/trader-table.css":
/*!***********************************!*\
  !*** ./src/view/trader-table.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".right {\r\n  text-align: right;\r\n}\r\n\r\n.oddWinner {\r\n  background-color: #efe;\r\n}\r\n\r\n.evenWinner {\r\n  background-color: #dfd;\r\n}\r\n\r\n.oddLoser {\r\n  background-color: #fee;\r\n}\r\n\r\n.evenLoser {\r\n  background-color: #fdd;\r\n}\r\n\r\ntable {\r\n  border-collapse: collapse;\r\n}\r\n\r\nthead th {\r\n  margin-bottom: 6px;\r\n  padding: 8px 6px;\r\n}\r\n\r\ntbody tr {\r\n  border: 1px solid grey;\r\n}\r\n\r\ntbody td {\r\n  padding: 8px 6px;\r\n}\r\n\r\ntbody td.narrow {\r\n  padding-left: 0;\r\n}\r\n\r\ntbody td.wide {\r\n  padding-left: 12px;\r\n}\r\n\r\ntbody td.indent {\r\n  padding-right: 12px;\r\n}\r\n\r\ntfoot td {\r\n  font-weight: bold;\r\n  margin-top: 8px;\r\n}\r\n\r\ntfoot td.indent {\r\n  padding-right: 12px;\r\n}\r\n\r\ninput[type=\"button\"] {\r\n  width: 72px;\r\n  background-color: white;\r\n  padding: 6px;\r\n}\r\n\r\ninput[type=\"image\"] {\r\n  height: 24px;\r\n  margin-top: 2px;\r\n  border: 2px outset #ccc;\r\n}\r\n\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy92aWV3L3RyYWRlci10YWJsZS5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxpQkFBaUI7QUFDbkI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSx5QkFBeUI7QUFDM0I7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0Usc0JBQXNCO0FBQ3hCOztBQUVBO0VBQ0UsZ0JBQWdCO0FBQ2xCOztBQUVBO0VBQ0UsZUFBZTtBQUNqQjs7QUFFQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLG1CQUFtQjtBQUNyQjs7QUFFQTtFQUNFLGlCQUFpQjtFQUNqQixlQUFlO0FBQ2pCOztBQUVBO0VBQ0UsbUJBQW1CO0FBQ3JCOztBQUVBO0VBQ0UsV0FBVztFQUNYLHVCQUF1QjtFQUN2QixZQUFZO0FBQ2Q7O0FBRUE7RUFDRSxZQUFZO0VBQ1osZUFBZTtFQUNmLHVCQUF1QjtBQUN6QiIsImZpbGUiOiJzcmMvdmlldy90cmFkZXItdGFibGUuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiLnJpZ2h0IHtcclxuICB0ZXh0LWFsaWduOiByaWdodDtcclxufVxyXG5cclxuLm9kZFdpbm5lciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2VmZTtcclxufVxyXG5cclxuLmV2ZW5XaW5uZXIge1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNkZmQ7XHJcbn1cclxuXHJcbi5vZGRMb3NlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZlZTtcclxufVxyXG5cclxuLmV2ZW5Mb3NlciB7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogI2ZkZDtcclxufVxyXG5cclxudGFibGUge1xyXG4gIGJvcmRlci1jb2xsYXBzZTogY29sbGFwc2U7XHJcbn1cclxuXHJcbnRoZWFkIHRoIHtcclxuICBtYXJnaW4tYm90dG9tOiA2cHg7XHJcbiAgcGFkZGluZzogOHB4IDZweDtcclxufVxyXG5cclxudGJvZHkgdHIge1xyXG4gIGJvcmRlcjogMXB4IHNvbGlkIGdyZXk7XHJcbn1cclxuXHJcbnRib2R5IHRkIHtcclxuICBwYWRkaW5nOiA4cHggNnB4O1xyXG59XHJcblxyXG50Ym9keSB0ZC5uYXJyb3cge1xyXG4gIHBhZGRpbmctbGVmdDogMDtcclxufVxyXG5cclxudGJvZHkgdGQud2lkZSB7XHJcbiAgcGFkZGluZy1sZWZ0OiAxMnB4O1xyXG59XHJcblxyXG50Ym9keSB0ZC5pbmRlbnQge1xyXG4gIHBhZGRpbmctcmlnaHQ6IDEycHg7XHJcbn1cclxuXHJcbnRmb290IHRkIHtcclxuICBmb250LXdlaWdodDogYm9sZDtcclxuICBtYXJnaW4tdG9wOiA4cHg7XHJcbn1cclxuXHJcbnRmb290IHRkLmluZGVudCB7XHJcbiAgcGFkZGluZy1yaWdodDogMTJweDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cImJ1dHRvblwiXSB7XHJcbiAgd2lkdGg6IDcycHg7XHJcbiAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbiAgcGFkZGluZzogNnB4O1xyXG59XHJcblxyXG5pbnB1dFt0eXBlPVwiaW1hZ2VcIl0ge1xyXG4gIGhlaWdodDogMjRweDtcclxuICBtYXJnaW4tdG9wOiAycHg7XHJcbiAgYm9yZGVyOiAycHggb3V0c2V0ICNjY2M7XHJcbn1cclxuXHJcbiJdfQ== */"

/***/ }),

/***/ "./src/view/trader-table.ts":
/*!**********************************!*\
  !*** ./src/view/trader-table.ts ***!
  \**********************************/
/*! exports provided: TraderTable */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TraderTable", function() { return TraderTable; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_trader_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/trader-service */ "./src/model/trader-service.ts");



/**
 * Angular component showing all current traders, including their
 * type, parameters, state, and profitability.
 *
 * @author Will Provost
 */
let TraderTable = class TraderTable {
    /**
     * Store the injected service references.
     */
    constructor(service) {
        this.traders = [];
        this.service = service;
        service.subscribe(this);
        service.notify();
    }
    /**
     * Helper to format times in mm:ss format.
     */
    minSec(millis) {
        let seconds = millis / 1000;
        const minutes = Math.floor(seconds / 60);
        seconds = seconds % 60;
        const pad = seconds < 10 ? "0" : "";
        return "" + minutes + ":" + pad + seconds;
    }
    /**
     * Replace our array of traders with the latest,
     * which will triger a UI update.
     */
    latestTraders(traders) {
        this.traders = traders;
    }
    /**
     * Helper to derive a label for the trader's state: "Started",
     * "Stopped", or, if deactivated but still closing out a position,
     * "Stopping".
     */
    getState(trader) {
        return trader.active ? "Started" : "Stopped";
    }
    /**
     * Helper to derive the total number of trades made by this trader.
     */
    getTotalTrades() {
        return this.traders.map(t => t.trades).reduce((x, y) => x + y, 0);
    }
    /**
     * Helper to derive the trader's total profit.
     */
    getTotalProfit() {
        return this.traders.map(t => t.profitOrLoss).reduce((x, y) => x + y, 0);
    }
    /**
     * Finds the trader at the given table index and uses the
     * TraderService component to send an HTTP request to toggle the
     * trader's state.
     */
    startOrStop(ev, hardStop) {
        const index = ev.target.id.replace("trader", "");
        const trader = this.traders[index];
        this.service.setActive(trader.ID, !trader.active);
    }
};
TraderTable = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "trader-table",
        template: __webpack_require__(/*! raw-loader!./trader-table.html */ "./node_modules/raw-loader/index.js!./src/view/trader-table.html"),
        styles: [__webpack_require__(/*! ./trader-table.css */ "./src/view/trader-table.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_model_trader_service__WEBPACK_IMPORTED_MODULE_2__["TraderService"]])
], TraderTable);



/***/ }),

/***/ "./src/view/trader-toolbar.css":
/*!*************************************!*\
  !*** ./src/view/trader-toolbar.css ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "div {\r\n  margin-bottom: 8px;\r\n}\r\n\r\nspan {\r\n  margin-left: 6px;\r\n}\r\n\r\ninput {\r\n  width: 40px;\r\n  margin-left: 2px;\r\n  margin-bottom: 4px;\r\n}\r\n\r\ninput[type=\"number\"] {\r\n  width: 50px;\r\n  text-align: right;\r\n}\r\n\r\ninput[type=\"button\"] {\r\n  width: auto;\r\n  margin-left: 8px;\r\n  padding: 6px;\r\n  background-color: #efe;\r\n  border: 2px outset #88f;\r\n}\r\n\r\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy92aWV3L3RyYWRlci10b29sYmFyLmNzcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFBQTtFQUNFLGtCQUFrQjtBQUNwQjs7QUFFQTtFQUNFLGdCQUFnQjtBQUNsQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsa0JBQWtCO0FBQ3BCOztBQUVBO0VBQ0UsV0FBVztFQUNYLGlCQUFpQjtBQUNuQjs7QUFFQTtFQUNFLFdBQVc7RUFDWCxnQkFBZ0I7RUFDaEIsWUFBWTtFQUNaLHNCQUFzQjtFQUN0Qix1QkFBdUI7QUFDekIiLCJmaWxlIjoic3JjL3ZpZXcvdHJhZGVyLXRvb2xiYXIuY3NzIiwic291cmNlc0NvbnRlbnQiOlsiZGl2IHtcclxuICBtYXJnaW4tYm90dG9tOiA4cHg7XHJcbn1cclxuXHJcbnNwYW4ge1xyXG4gIG1hcmdpbi1sZWZ0OiA2cHg7XHJcbn1cclxuXHJcbmlucHV0IHtcclxuICB3aWR0aDogNDBweDtcclxuICBtYXJnaW4tbGVmdDogMnB4O1xyXG4gIG1hcmdpbi1ib3R0b206IDRweDtcclxufVxyXG5cclxuaW5wdXRbdHlwZT1cIm51bWJlclwiXSB7XHJcbiAgd2lkdGg6IDUwcHg7XHJcbiAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbn1cclxuXHJcbmlucHV0W3R5cGU9XCJidXR0b25cIl0ge1xyXG4gIHdpZHRoOiBhdXRvO1xyXG4gIG1hcmdpbi1sZWZ0OiA4cHg7XHJcbiAgcGFkZGluZzogNnB4O1xyXG4gIGJhY2tncm91bmQtY29sb3I6ICNlZmU7XHJcbiAgYm9yZGVyOiAycHggb3V0c2V0ICM4OGY7XHJcbn1cclxuIl19 */"

/***/ }),

/***/ "./src/view/trader-toolbar.ts":
/*!************************************!*\
  !*** ./src/view/trader-toolbar.ts ***!
  \************************************/
/*! exports provided: TraderToolbar */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TraderToolbar", function() { return TraderToolbar; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm2015/core.js");
/* harmony import */ var _model_two_moving_averages__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../model/two-moving-averages */ "./src/model/two-moving-averages.ts");
/* harmony import */ var _model_trader_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../model/trader-service */ "./src/model/trader-service.ts");




/**
 * Angular component for a toolbar that allows the user to configure
 * and to create new traders. The component logic and the HTML template
 * currently support only the 2MA trader type.
 *
 * @author Will Provost
 */
let TraderToolbar = class TraderToolbar {
    /**
     * Set default values for all properties, which will flow out to the
     * initial UI via two-way binding.
     */
    constructor(service) {
        this.service = service;
        this.type = "2MA";
        this.stock = "MRK";
        this.size = 1000;
        this.lengthShort = 30;
        this.lengthLong = 60;
        this.exitThreshold = 3;
    }
    /**
     * Reads the values of form controls via two-way binding.
     * Creates an instance of the trader (only 2MA traders currently supported)
     * and sends it to the server to be created and activated.
     */
    create() {
        this.service.createTrader(new _model_two_moving_averages__WEBPACK_IMPORTED_MODULE_2__["TwoMovingAverages"](0, this.stock, this.size, true, false, [], 0, NaN, this.lengthShort * 1000, this.lengthLong * 1000, this.exitThreshold / 100));
    }
};
TraderToolbar = tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"]([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: "trader-toolbar",
        template: __webpack_require__(/*! raw-loader!./trader-toolbar.html */ "./node_modules/raw-loader/index.js!./src/view/trader-toolbar.html"),
        styles: [__webpack_require__(/*! ./trader-toolbar.css */ "./src/view/trader-toolbar.css")]
    }),
    tslib__WEBPACK_IMPORTED_MODULE_0__["__metadata"]("design:paramtypes", [_model_trader_service__WEBPACK_IMPORTED_MODULE_3__["TraderService"]])
], TraderToolbar);



/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\Administrator\Documents\Workplace\HFProject\Indigo\HFTraderWeb\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main-es2015.js.map